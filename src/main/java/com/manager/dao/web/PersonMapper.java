package com.manager.dao.web;

import com.manager.common.dto.web.PersonDTO;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface PersonMapper {
    @Insert("insert into t_person (name,imgurl,detail,sort,positions) values (#{name},#{imgurl},#{detail},#{sort},#{positions})")
    public int insert(PersonDTO personDTO);

    @Delete("delete from t_person where id=#{id}")
    public int delete(Integer id);

    @Select("select * from t_person order by sort asc")
    public List<Map<String,Object>> queryAll();

    @Select("select * from t_person where id=#{id}")
    public Map<String,Object> queryById(Integer id);
}
