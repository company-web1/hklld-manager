package com.manager.dao.web;

import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface JoinUsMapper {
    @Select("select * from t_join_us order by sort asc")
    public List<Map<String,Object>> queryAll();
    @Insert("insert into t_join_us (name,detail,sort) values (#{name},#{detail},#{sort})")
    public int insert(Map<String,Object> map);
    @Delete("delete from t_join_us where id=#{id}")
    public int delete(Integer id);

    @Delete("select * from t_join_us where id=#{id}")
    public Map<String,Object> queryById(Integer id);
}
