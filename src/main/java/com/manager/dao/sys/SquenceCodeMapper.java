package com.manager.dao.sys;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.manager.common.dto.sys.SquenceCodeDTO;

public interface SquenceCodeMapper extends BaseMapper<SquenceCodeDTO> {
	
    int getSquenceCode(String code);

}
