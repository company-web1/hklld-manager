package com.manager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.manager.common.dto.web.ProductDTO;
import com.manager.common.util.Assist;
import com.manager.common.util.Contant;
import com.manager.dao.web.ProductMapper;
import com.manager.service.web.IProductService;
import com.manager.web.controller.utils.AjaxUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * 服务产品管理
 * @author admin
 *
 */
@Controller
@RequestMapping("project")
public class ProjectController {
	
	@Autowired
	private IProductService productService;

	@Autowired
	private ProductMapper productMapper;
	
	/**
	 * 服务产品列表管理
	 * @param model
	 * @param current
	 * @return
	 */
	@RequestMapping("show")
	public String show(Model model, String current,HttpServletRequest request) {
		String menuId = request.getParameter("menuId");
		Assist assist = new Assist();
		assist.setOrder(Assist.order("sort", true));
        assist.setOrder(Assist.order("create_time", false));
        assist.setRequires(Assist.andNeq("is_del", "1"));
        assist.setRequires(Assist.andEq("type", "1"));
        assist.setRowSize(Contant.NONMAL_PAGE_SIZE);
		assist.setRequires(Assist.andEq("parent_id", Integer.parseInt(menuId)));
        if(StringUtils.isEmpty(current)) {
        	current = "0";
        }
        assist.setStartRow(Integer.parseInt(current));
		List<ProductDTO> productList = productService.selectProductDTO(assist);
		long total = productService.getProductDTORowCount(assist);
        Page<ProductDTO> result = new Page<ProductDTO>();
        result.setRecords(productList);
        result.setSize(Contant.NONMAL_PAGE_SIZE);
        result.setTotal((int)total);
        result.setCurrent(Integer.parseInt(current));
        model.addAttribute("result", result);
        model.addAttribute("menuId",menuId);
		return "project/show";
	}
	
	/**
	 * 服务产品新增/修改
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("input")
	public String input(String id, Model model,Integer menuId) {

		if(StringUtils.isNotEmpty(id)) {
			ProductDTO productDTO = productService.selectProductDTOById(Long.parseLong(id));
			model.addAttribute("productDTO", productDTO);
			model.addAttribute("menuId",productDTO.getParentId());
		}else{
			model.addAttribute("menuId", menuId);
		}
		return "project/input";
	}
	@RequestMapping("input2")
	public String input2(String id, Model model) {

		if(StringUtils.isNotEmpty(id)) {
			ProductDTO productDTO = productService.selectProductDTOById(Long.parseLong(id));
			model.addAttribute("productDTO", productDTO);
		}
		return "project/input2";
	}
	@RequestMapping("input3")
	public String input3(String id, Model model) {

		if(StringUtils.isNotEmpty(id)) {
			ProductDTO productDTO = productService.selectProductDTOById(Long.parseLong(id));
			model.addAttribute("productDTO", productDTO);
		}
		return "project/input3";
	}
	@RequestMapping("updateShortDetails")
	public void updateShortDetails(ProductDTO ProductDTO, HttpServletResponse response ){
		int n = 0;
		if(StringUtils.isNotEmpty(ProductDTO.getShortDetails())) {
			if(ProductDTO.getId() > 0) {
				n = productMapper.updateShortDetails(ProductDTO);
			}
		}
		AjaxUtil.ajaxJsonSucMessage(response, n);
	}
	@RequestMapping("updateEnDetails")
	public void updateEnDetails(ProductDTO ProductDTO, HttpServletResponse response ){
		int n = 0;
		if(StringUtils.isNotEmpty(ProductDTO.getEnDetails())) {
			if(ProductDTO.getId() > 0) {
				n = productMapper.updateEnDetails(ProductDTO);
			}
		}
		AjaxUtil.ajaxJsonSucMessage(response, n);
	}
	/**
	 * 服务产品保存
	 * @param ProductDTO
	 * @param response
	 */
	@RequestMapping("save")
	public void save (ProductDTO ProductDTO, HttpServletResponse response, HttpServletRequest request) {
		int n = 0;
		if(StringUtils.isNotEmpty(ProductDTO.getName())) {
			ProductDTO.setUpdateTime(new Date());
			if(ProductDTO.getId() > 0) {
				n = productService.updateNonEmptyProductDTOById(ProductDTO);
			} else {
				ProductDTO.setType("1");
				ProductDTO.setCreateTime(new Date());
				ProductDTO.setIsDel("0");
				n = productService.insertProductDTO(ProductDTO);
			}
		}
//		WebInfoFilter.flag = 1;
		AjaxUtil.ajaxJsonSucMessage(response, n);
	}
	
	/**
	 * 服务产品删除
	 * @param id
	 * @param response
	 */
	@RequestMapping("del")
	public void del(long id, HttpServletResponse response) {
		ProductDTO ProductDTO = productService.selectProductDTOById(id);
		int n = 1;
		if(ProductDTO != null) {
			ProductDTO.setIsDel("1");
			n = productService.updateProductDTOById(ProductDTO);
		}
		AjaxUtil.ajaxJsonSucMessage(response, n);
	}

}
