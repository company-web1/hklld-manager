package com.manager.controller;

import com.manager.common.dto.web.PersonDTO;
import com.manager.dao.web.JoinUsMapper;
import com.manager.web.controller.utils.AjaxUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Controller
public class ContactController {
    @Autowired
    JoinUsMapper joinUsMapper;
    @RequestMapping("/join/show")
    public String show(Model model,HttpServletRequest request){
        String menuId = request.getParameter("menuId");
        List<Map<String,Object>> result = joinUsMapper.queryAll();
        model.addAttribute("result",result);
        model.addAttribute("menuId",menuId);
        return "join/show";
    }
    @RequestMapping("/join/input")
    public String input(String id,Model model){
        if(id!=null){
            Map<String,Object> map = joinUsMapper.queryById(Integer.parseInt(id));
            model.addAttribute("result",map);
        }
        return "join/input";
    }
    @RequestMapping("/join/save")
    public void personSave(Model model, Map<String,Object> objectMap, HttpServletResponse response, HttpServletRequest request){
        int n = 0;
        if(StringUtils.isNotEmpty(String.valueOf(objectMap.get("name")))) {
            if(objectMap.get("id").equals("0")){
                joinUsMapper.delete(Integer.parseInt(objectMap.get("id").toString()));
            }
            n = joinUsMapper.insert(objectMap);
        }
        AjaxUtil.ajaxJsonSucMessage(response, n);
    }
    @RequestMapping("/join/del")
    public void personDel(Model model, HttpServletRequest request, HttpServletResponse response){
        String id = request.getParameter("id");
        int i = joinUsMapper.delete(Integer.parseInt(id));
        AjaxUtil.ajaxJsonSucMessage(response, i);
    }
}
