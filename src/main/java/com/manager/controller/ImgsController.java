package com.manager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.manager.common.FileUploadQueue;
import com.manager.common.dto.web.BannerDTO;
import com.manager.common.util.Assist;
import com.manager.common.util.Contant;
import com.manager.service.web.IBannerService;
import com.manager.web.controller.utils.AjaxUtil;
import com.manager.web.controller.utils.SaveUploadFile;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * 图片管理
 * @author admin
 *
 */
@Controller
@RequestMapping("project/imgs")
public class ImgsController {
	
	@Autowired
	private IBannerService bannerService;
	
	/**
	 * banner列表
	 * @param model
	 * @param current
	 * @return
	 */
	@RequestMapping("show")
	public String show (Model model, String current,HttpServletRequest request) {
		String menuId = request.getParameter("menuId");
		String projectId = request.getParameter("projectId");
		Assist assist = new Assist();
		assist.setOrder(Assist.order("sort", true));
        assist.setOrder(Assist.order("create_time", false));
        assist.setRequires(Assist.andNeq("is_del", "1"));
		assist.setRowSize(Contant.NONMAL_PAGE_SIZE);
        if(StringUtils.isNotEmpty(menuId)){
			assist.setRequires(Assist.andEq("menuId", Integer.parseInt(menuId)));
		}
		if(StringUtils.isNotEmpty(projectId) ){ // 不为空是是具体项目的图片
			assist.setRequires(Assist.andEq("parentId", Integer.parseInt(projectId)));
		}
        if(StringUtils.isEmpty(current)) {
        	current = "0";
        }
        assist.setStartRow(Integer.parseInt(current));
        List<BannerDTO> bannerList = bannerService.selectBannerDTO(assist);
        long total = bannerService.getBannerDTORowCount(assist);
        Page<BannerDTO> result = new Page<BannerDTO>();
        result.setRecords(bannerList);
        result.setSize(Contant.NONMAL_PAGE_SIZE);
        result.setTotal((int)total);
        result.setCurrent(Integer.parseInt(current));
		model.addAttribute("result", result);
        model.addAttribute("menuId", menuId);
		model.addAttribute("projectId", projectId);
		return "imgs/show";
	}
	
	/**
	 * 新增/修改
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("input")
	public String input(String id, Model model,HttpServletRequest request) {
		String menuId = request.getParameter("menuId");
		String projectId = request.getParameter("projectId");
		if(StringUtils.isNotEmpty(id)) {
			BannerDTO bannerDTO = bannerService.selectBannerDTOById(Long.parseLong(id));
			model.addAttribute("bannerDTO", bannerDTO);
		}
		model.addAttribute("menuId", menuId);
		model.addAttribute("projectId", projectId);
		return "imgs/input";
	}
	
	/**
	 * 保存
	 * @param bannerDTO
	 * @param response
	 */
	@RequestMapping("save")
	public void save (BannerDTO bannerDTO, HttpServletResponse response, HttpServletRequest request) throws IOException {
		String fileName  = request.getParameter("fileName");
		int n = 0;
		if(StringUtils.isNotEmpty(fileName)){ //不为空，就重新上传图，删除以前的旧的
			String bannerUrl = bannerDTO.getBannerUrl();
			String oldPic="";
			if(StringUtils.isNotEmpty(bannerUrl)){
				String fileNameArray [] = bannerUrl.split("/");
				oldPic = fileNameArray[fileNameArray.length - 1];
				SaveUploadFile.deletePicture(oldPic);// 删除以前的旧的
			}
			bannerUrl = "http://www.hklld.com/image/hklld/"+fileName;
			bannerDTO.setBannerUrl(bannerUrl);
		}
		bannerDTO.setUpdateTime(new Date());
		if(bannerDTO.getId() > 0) {
			n = bannerService.updateNonEmptyBannerDTOById(bannerDTO);
		} else {
			bannerDTO.setCreateTime(new Date());
			bannerDTO.setIsDel("0");
			n = bannerService.insertBannerDTO(bannerDTO);
		}
//		WebInfoFilter.flag = 1;
		AjaxUtil.ajaxJsonSucMessage(response, n);
	}
	
	/**
	 * 删除
	 * @param id
	 * @param response
	 */
	@RequestMapping("del")
	public void del(long id, HttpServletResponse response) {
		BannerDTO bannerDTO = bannerService.selectBannerDTOById(id);
		int n = 1;
		if(bannerDTO != null) {
			bannerDTO.setIsDel("1");
			n = bannerService.updateBannerDTOById(bannerDTO);
		}
		AjaxUtil.ajaxJsonSucMessage(response, n);
	}

}
