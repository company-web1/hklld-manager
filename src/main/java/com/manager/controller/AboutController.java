package com.manager.controller;

import com.manager.common.dto.web.PersonDTO;
import com.manager.dao.web.PersonMapper;
import com.manager.web.controller.utils.AjaxUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class AboutController {
    @Autowired
    PersonMapper personMapper;
    @RequestMapping("/person/show")
    public String personShow(Model model, HttpServletRequest request){
        List<Map<String,Object>> personList = personMapper.queryAll();
        model.addAttribute("personList",personList);
        return "person/show";
    }
    @RequestMapping("/person/input")
    public String personInput(Model model, HttpServletRequest request){
        String id = request.getParameter("id");
        if(id!=null){ // 修改
            Map<String,Object> personDTO = personMapper.queryById(Integer.parseInt(id));
           model.addAttribute("person",personDTO);
        }
        return "person/input";
    }
    @RequestMapping("/person/save")
    public void personSave(Model model,PersonDTO personDTO,HttpServletResponse response, HttpServletRequest request){
        int n = 0;
        if(StringUtils.isNotEmpty(personDTO.getName())) {
            if(personDTO.getId()!=0){
                personMapper.delete(personDTO.getId());
            }
            n = personMapper.insert(personDTO);
        }
        AjaxUtil.ajaxJsonSucMessage(response, n);
    }
    @RequestMapping("/person/del")
    public void personDel(Model model, HttpServletRequest request, HttpServletResponse response){
        String id = request.getParameter("id");
        int i = personMapper.delete(Integer.parseInt(id));
        AjaxUtil.ajaxJsonSucMessage(response, i);
    }
}
