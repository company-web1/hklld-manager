package com.manager.common.dto.web;

public class PersonDTO {
    private Integer id;
    private String name;
    private String imgurl;
    private String positions;
    private String detail;
    private Integer sort;

    public PersonDTO(Integer id, String name, String imgurl, String positions, String detail, Integer sort) {
        this.id = id;
        this.name = name;
        this.imgurl = imgurl;
        this.positions = positions;
        this.detail = detail;
        this.sort = sort;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getPositions() {
        return positions;
    }

    public void setPositions(String positions) {
        this.positions = positions;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
