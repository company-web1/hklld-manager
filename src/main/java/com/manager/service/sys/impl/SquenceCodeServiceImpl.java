package com.manager.service.sys.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manager.dao.sys.SquenceCodeMapper;
import com.manager.service.sys.ISquenceCodeService;

@Service
public class SquenceCodeServiceImpl implements ISquenceCodeService {
	
	@Autowired
	private SquenceCodeMapper squenceCodeDAO;

	public int getSquenceCode(String code) {
		return squenceCodeDAO.getSquenceCode(code);
	}

}
