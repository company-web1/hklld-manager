package com.manager.service.sys;

public interface ISquenceCodeService {
	
	int getSquenceCode(String code);

}
