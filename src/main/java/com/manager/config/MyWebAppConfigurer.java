package com.manager.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/*import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration; 这个在新版本过时了*/

//@Configuration  // 注释掉，不然swagger 起不来
public class MyWebAppConfigurer extends WebMvcConfigurationSupport {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 可以添加多个，组成拦截器链
        registry.addInterceptor(new MyInterceptor1()).addPathPatterns("/")
                .excludePathPatterns();
        super.addInterceptors(registry);
    }
}
