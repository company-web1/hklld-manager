package com.manager.config;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyInterceptor1 implements HandlerInterceptor {
    // 在响应之前执行
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        System.out.println("在 请求处理之前进入拦截器，也就是调用controller的方法之前！");
        return true;
    }

    // 在响应完成后，只有 preHandle 返回 true 才执行
    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        System.out.println("在请求处理完成之后，进入拦截器");
    }

    // 在视图渲染完成后 只有 preHandle 返回 true 才执行，主要用于资源清理工作
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        System.out.println("在视图渲染完成后进入拦截器方法");
    }
}
